<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/


use Firebase\JWT\JWT ;  


$router->get('/', function () use ($router) {
    return $router->app->version();
});



//ดูเวลาการเข้าออกการทำงาน
$router->get('/list_time', function () {
    $results = app('db')->select("SELECT * FROM eprecord");
    return response()->json($results);
});

//ลงเวลาเข้างาน
$router->post('/add_time',function(Illuminate\Http\Request $request){
    $timeout = strtotime($request->input("timeout"));
	$timeout = date('Y-m-d H:i:s', $timeout);
    $timein = strtotime($request->input("timein"));
	$timein = date('Y-m-d H:i:s', $timein);
    $query =app('db')->insert('INSERT into eprecord 
                                (Timein,Time_out)
                                 VALUE (?,Timein)',
                                [
                                $timein,$timeout
                                ] );

       return "Ok";
    
});
//ลงเวลาออกงาน
$router->put('/update_time', function(Illuminate\Http\Request $request) {
    $workid = $request->input("workid");
    $timeout = strtotime($request->input("timeout"));
	$timeout = date('Y-m-d H:i:s', $timeout);
    $query =app('db')->update('UPDATE eprecord  SET Time_out=?
                                 WHERE
                                    WorkID=?',
                                [$workid,
                                $timeout
                                ] );

       return "Ok";

 });
//ลบเวลาการทำงานทั้งวันของวันนั้น
 $router->delete('/delete_datatime', function(Illuminate\Http\Request $request) {
        
    $workid = $request->input("workid");
    $query = app('db')->delete('DELETE FROM  eprecord
                            
                                WHERE
                                WorkID=?',
                                    [$workid] );
        
        return "Ok";	

 });


//ดูรายชื่อทั้งหมด
 $router->get('/list_employee', function () {
	
	$results = app('db')->select("SELECT * FROM epinformation");
	return response()->json($results);
});

 //สมัครเข้าใช้งาน
$router->post('/register', function(Illuminate\Http\Request $request) {
	
        $name = $request->input("name");
        $surname = $request->input("surname");
        $username = $request->input("username");
        $password = app('hash')->make($request->input("password"));
        
        $query = app('db')->insert('INSERT into epinformation
                        (Username, Password, Name, Surname)
                        VALUE (?, ?, ?, ?)',
                        [ $username,
                          $password,
                          $name,
                          $surname
                          ] );
        return "Ok";
    });


//Login

 $router->post('/login', function(Illuminate\Http\Request $request) {
	
	$username = $request->input("username");
	$password = $request->input("password");
	
	$result = app('db')->select("SELECT Password FROM epinformation  WHERE Username=?",
									[$username]);
									
	$loginResult = new stdClass();
	
	if(count ($result) == 0) {
			$loginResult->status = "fail";
			$loginResult->reason = "User is not founded";
	}else {
			$loginResult->status = "success";
		}
	
});